// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 10.9.2
let package = Package(
    name: "FreestarAds-Googleadmob",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-Googleadmob",
            targets: [
                "FreestarAds-Googleadmob",
                "FreestarAds-Googleadmob-Core",
                "FreestarAds-GMA-Dependencies-Admob-Wrapper"
                ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-gma-dependencies.git",
            exact: "10.9.0"
            ),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-core.git",
            from: "5.30.0"
            )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "FreestarAds-Googleadmob",
            url: "https://gitlab.com/freestar/spm-freestarads-admob/-/raw/10.9.2/FreestarAds-Googleadmob.xcframework.zip",
            checksum: "5e42ec5be949091cbb692939260eb0e28a5ef3d908d54bfee033ffa05ce6511a"
            ),
        .target(
            name: "FreestarAds-Googleadmob-Core",
                dependencies: [
                    .product(name: "FreestarAds", package: "spm-freestarads-core")
                ]
            ),
        .target(
            name: "FreestarAds-GMA-Dependencies-Admob-Wrapper",
                dependencies: [
                    .product(name: "FreestarAds-GMA-Dependencies", package: "spm-freestarads-gma-dependencies")
                ]
            ),
    ]
)
